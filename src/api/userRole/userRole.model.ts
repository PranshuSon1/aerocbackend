import { Table, Model, Column, DataType } from "sequelize-typescript";


@Table({
    timestamps: true,
    tableName: "usersRoles",
  })
  export class usersRoles extends Model { 
    @Column({
        type: DataType.STRING,
      //  defaultValue: true,
      })
      name!: string;
    @Column({
        type : DataType.STRING,
    })
    RoleDescription!: string
    @Column({
        type : DataType.BOOLEAN
    })
    isActive !: boolean


  }