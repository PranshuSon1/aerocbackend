import { Request,Response,NextFunction } from "express";
import { create,getall } from "./customer.service";

export const addCustomer = async (req :Request, res:Response) : Promise<void> =>{
    await create(req,(error : any , result :any)=>{
        if(error){
            res.status(200).json({error : error.message})
        }else{
            res.json({result})
        }
    })
}
export const getAll = async (req :Request, res:Response) : Promise<void> =>{
    await getall(req,(error : any , result :any)=>{
        if(error){
            res.status(200).json({error : error.message})
        }else{
            res.json({result})
        }
    })
}