
import { plants } from "./plants.models";
import logging from "../../config/logging";
const NAMESPACE = 'Vendor'
export const createPlant = async (req : any,callBack :any) :Promise<any> =>{
    try {
        let data = {...req.body};
        let plantData = await plants.create(data)
        await plantData.save();
        return callBack(null,{message : 'created Successfully', data: plantData})
    } catch (error :any) {
        console.log('error', error)
        return callBack(true,error)
    }
}

export const getAllPlants = async (req:any,callBack:any) :Promise<any> =>{
    try {
        let data = await plants.findAll({})
        return callBack(null,{data:data})
    } catch (error :any) {
        console.log('error', error)
        return callBack(true,error)
    }
}

export const updatePlants = async (req:any , callBack:any):Promise<any> =>{
    try {
        console.log('Update plant')
        return callBack ( null , { data :"update plants!"})
    } catch (error) {
        console.log('error', error)
        return callBack(true , error)
    }
}

