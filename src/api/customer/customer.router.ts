import { Router } from "express";
import validateUser from "../../common/middilewares/validateUser";
import { create,getall } from "./customer.service";
const customerRouter = Router();

customerRouter.get('/getall',validateUser,getall);
customerRouter.post('/add',validateUser,create);

export default customerRouter