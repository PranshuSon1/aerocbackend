import { Table, Model, Column, DataType } from "sequelize-typescript";

@Table({
    timestamps: true,
    tableName: "customer",
  })
  export class customer extends Model { 
    @Column({
        type: DataType.STRING,
      })
      customerName!: string;
    @Column({
        type: DataType.STRING,
      })
      customerDisplayName!: string;
    @Column({
        type: DataType.STRING,
      })
      customerAddressLine1!: string;
    @Column({
        type: DataType.STRING,
      })
      customerAddressLine2!: string;
    @Column({
        type: DataType.STRING,
      })
      customerCity!: string;
    @Column({
        type: DataType.STRING,
      })
      customerState!: string;
    @Column({
        type: DataType.STRING,
      })
      customerCountry!: string;
    @Column({
        type: DataType.STRING,
      })
      customerContractNumber!: string;
    @Column({
        type: DataType.STRING,
      })
      customerEmailId!: string;
    @Column({
        type: DataType.BOOLEAN,
      })
      isActive!: boolean;
    @Column({
        type: DataType.STRING,
      })
      createdBy!: string;
    @Column({
        type: DataType.STRING,
      })
      updatedBy!: string;
  }