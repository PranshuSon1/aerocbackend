import { Table, Model, Column, DataType } from "sequelize-typescript";


@Table({
    timestamps: true,
    tableName: "users",
  })
  export class Users extends Model {
    @Column({
      type: DataType.BOOLEAN,
      defaultValue: true,
    })
    isActive!: boolean;
    @Column({
        type : DataType.STRING,
        // allowNull   : false
    })
    userCredentials!: string;     // <= password
    @Column({
        type : DataType.STRING,
        // allowNull   : false
    })
    userName !: string;     // <= username
    @Column({
        type : DataType.STRING,
        // allowNull   : false
    })
    userFirstName!: string;
    @Column({
        type : DataType.STRING,
        // allowNull   : false
    })
    userMiddleName!: string;
    @Column({
        type : DataType.STRING,
        // allowNull   : false
    })
    userLastName!: string;
    @Column({
        type : DataType.STRING,
        // allowNull   : false
    })
    userPhone!: string;
    @Column({
        type : DataType.STRING,
        // allowNull   : false
    })
    userEmail!: string;
    @Column({
        type : DataType.STRING,
        // allowNull   : false
    })
    userRoleId!: string;   // forien key from role table
    @Column({
        type : DataType.STRING,
        // allowNull   : false
    })
    userProfilePicture!: string;
    @Column({
        type : DataType.DATE,
        // allowNull   : false
    })
    userStartDate!: Date;
    @Column({
        type : DataType.DATE,
        // allowNull   : false
    })
    userEndDate!: Date;
    @Column({
        type : DataType.BOOLEAN,
        // allowNull   : false
    })
    isPhoneConfirmed!: boolean;
    @Column({
        type : DataType.BOOLEAN,
      //  allowNull   : false
    })
    isEmailConfirmed!: boolean;
    @Column({
        type : DataType.BOOLEAN,
      //  allowNull   : false
    })
    isLockedOut!: boolean;
    @Column({
        type : DataType.STRING,
      //  allowNull   : false
    })
    createdBy!: String;
    @Column({
        type : DataType.STRING,
      //  allowNull   : false
    })
    updatedBy!: String;
  }