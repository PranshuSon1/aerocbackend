import { vendors ,vendorType } from "./vendor.model";
import logging from "../../config/logging";
const NAMESPACE = 'Vendor'
export const createVendor = async (req : any,callBack :any) :Promise<any> =>{
    try {
        let data = {...req.body};
        let vendorData = await vendors.create(data)
        await vendorData.save();
        return callBack(null,{message : 'created Successfully', data: vendorData})
    } catch (error :any) {
        console.log('error', error)
        return callBack(true,error)
    }
}

export const getallVendor = async (req:any,callBack:any) :Promise<any> =>{
    try {
        let data = await vendors.findAll({})
        return callBack(null,{data:data})
    } catch (error :any) {
        console.log('error', error)
        return callBack(true,error)
    }
}

export const updateVendor = async (req:any , callBack:any):Promise<any> =>{
    try {
        console.log('Update Vendor')
        return callBack ( null , { data :"update Vendor!"})
    } catch (error) {
        console.log('error', error)
        return callBack(true , error)
    }
}

