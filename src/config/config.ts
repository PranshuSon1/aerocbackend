import dotenv from 'dotenv';

dotenv.config();

// PORT = 8080
// APP_SECRET = ae_RocBackendApp
// DATABASE_HOST = 65.0.93.166
// DATABASE_PORT = 5432
// DATABASE_DIALECT = postgres
// DATABASE_NAME = 'aeroc_stgdb1'
// DATABASE_USER = 'aeroc_stg'
// DATABASE_PASSWORD = 'aeroc_stg_coreco'
const DATABASE = {
    port : process.env.DATABASE_PORT,
    host : process.env.DATABASE_HOST,
    dialect : process.env.DATABASE_DIALECT,
    name : process.env.DATABASE_NAME,
    user : process.env.DATABASE_USER,
    password : process.env.DATABASE_PASSWORD,
    logging :false
}


const SERVER_HOSTNAME = process.env.SERVER_HOSTNAME //|| 'localhost';
const SERVER_PORT = process.env.PORT //|| 8080;
const SERVER_TOKEN_EXPIRETIME = process.env.SERVER_TOKEN_EXPIRETIME || 3600;
const SERVER_TOKEN_ISSUER = process.env.SERVER_TOKEN_ISSUER || 'coolIssuer';
const SERVER_TOKEN_SECRET = process.env.SERVER_TOKEN_SECRET || 'superencryptedsecret';

const SERVER = {
    hostname: SERVER_HOSTNAME,
    port: SERVER_PORT,
    token: {
        expireTime: SERVER_TOKEN_EXPIRETIME,
        issuer: SERVER_TOKEN_ISSUER,
        secret: SERVER_TOKEN_SECRET
    },
};
const config = {
    sql: DATABASE,
    server: SERVER
};
export default config;