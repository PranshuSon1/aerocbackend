import cors from 'cors';
import { Router ,Request, Response} from 'express';
import logging from '../config/logging';
//get router
const NAMESPACE = 'Server';
var router = Router();
// const UserRoutes = require('../api/user/user.router')
import userRoutes from '../api/user/user.router';
import vendorRouter from '../api/vendor/vendor.router';
import customerRouter from '../api/customer/customer.router';
import plantsRouter from '../api/plants/plants.router';
//options for cors midddleware
const options: cors.CorsOptions = {
  allowedHeaders: [
    'Origin',
    'X-Requested-With',
    'Content-Type',
    'Accept',
    'X-Access-Token',
  ],
  credentials: true,
  methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
//   origin: API_URL,
  preflightContinue: false,
};

//use cors middleware
router.use(cors(options));


/** Log the request */
router.use((req, res, next) => {
  /** Log the req */
  logging.info(NAMESPACE, `METHOD: [${req.method}] - URL: [${req.url}] - IP: [${req.socket.remoteAddress}]`);

  res.on('finish', () => {
      /** Log the res */
      logging.info(NAMESPACE, `METHOD: [${req.method}] - URL: [${req.url}] - STATUS: [${res.statusCode}] - IP: [${req.socket.remoteAddress}]`);
  });

  next();
});

//add your routes
router.use('/user',userRoutes);
router.use('vendors',vendorRouter);
router.use('/customers',customerRouter);
router.use('/plants',plantsRouter);

//enable pre-flight
router.options('*', cors(options));

router.get('/',(req:Request,res:Response)=>{
  res.send('SSM API is running!')
})

router.use((req, res, next) => {
  const error = new Error('Not found');

  res.status(404).json({
      message: error.message
  });
});



export default router;
