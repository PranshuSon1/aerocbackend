import { Table, Model, Column, DataType } from "sequelize-typescript";
// import { Col } from "sequelize/types/utils";


@Table({
  timestamps: true,
  tableName: "plants",
})
export class plants extends Model {
  @Column({
    type: DataType.STRING,
    //  defaultValue: true,
  })
  plantName!: string;
  @Column({
    type: DataType.STRING,
  })
  plantLocation!: string
  @Column({
    type: DataType.STRING
  })
  plantAddressLine1 !: string
  @Column({
    type: DataType.STRING
  })
  plantAddressLine2 !: string
  @Column({
    type: DataType.STRING
  })
  plantCity !: string
  @Column({
    type: DataType.INTEGER
  })
  plantPostalCode !: Number
  @Column({
    type: DataType.STRING
  })
  plantState !: string
  @Column({
    type: DataType.STRING
  })
  plantCountry !: string
  @Column({
    type: DataType.ARRAY(DataType.STRING)
  })
  plantphotos !: Array<any>
  @Column({
    type: DataType.STRING
  })
  plantTypeId !: string
  @Column({
    type: DataType.STRING
  })
  plantLatitude !: string
  @Column({
    type: DataType.STRING
  })
  plantLongitude !: string
  @Column({
    type: DataType.TINYINT
  })
  plantmonitoringFlag !: number
  @Column({
    type: DataType.STRING
  })
  ExpectedGeneration !: string
  @Column({
    type: DataType.TINYINT
  })
  isActive !: Number
  @Column({
    type: DataType.STRING
  })
  createdBy !: string
  @Column({
    type: DataType.STRING
  })
  updatedBy !: string

}
@Table({
  timestamps: true,
  tableName: "plantLocationType",
})
export class plantLocationType extends Model { 
  @Column({
      type: DataType.STRING,
    //  defaultValue: true,
    })
    plantLocationTypeName!: string;
  @Column({
      type : DataType.STRING,
  })
  plantLocationTypeDescription!: string
  @Column({
      type : DataType.BOOLEAN
  })
  isActive !: boolean
}
@Table({
  timestamps: true,
  tableName: "plantType",
})
export class plantType extends Model { 
  @Column({
      type: DataType.STRING,
    //  defaultValue: true,
    })
    plantTypeName!: string;
  @Column({
      type : DataType.STRING,
  })
  plantTypeDescription!: string
  @Column({
      type : DataType.BOOLEAN
  })
  isActive !: boolean
}

@Table({
  timestamps: true,
  tableName: "siteManager",
})
export class siteManager extends Model { 
  @Column({
      type: DataType.STRING,
    //  defaultValue: true,
    })
    plantTypeName!: string;
  @Column({
      type : DataType.STRING,
  })
  plantTypeDescription!: string
  @Column({
      type : DataType.BOOLEAN
  })
  isActive !: boolean
}
@Table({
  timestamps: true,
  tableName: "consumptionType",
})
export class consumptionType extends Model { 
  @Column({
      type: DataType.STRING,
    //  defaultValue: true,
    })
    plantTypeName!: string;
  @Column({
      type : DataType.STRING,
  })
  plantTypeDescription!: string
  @Column({
      type : DataType.BOOLEAN
  })
  isActive !: boolean
}
@Table({
  timestamps: true,
  tableName: "module",
})
export class _module extends Model { 
  @Column({
      type: DataType.STRING,
    //  defaultValue: true,
    })
    make!: string;
  @Column({
      type : DataType.STRING,
  })
  model!: string
  @Column({
      type : DataType.STRING,
  })
  technology!: string
  @Column({
      type : DataType.STRING,
  })
  BISNumber!: string
  @Column({
      type : DataType.STRING,
  })
  listOfSerialNumbers!: string
  @Column({
      type : DataType.STRING,
  })
  ratedPower!: string
  @Column({
      type : DataType.STRING,
  })
  thermalDe_ratingFactor!: string
  @Column({
      type : DataType.STRING,
  })
  anualDe_gradationFactor!: string
  @Column({
      type : DataType.STRING,
  })
  panelArea!: string
  @Column({
      type : DataType.STRING,
  })
  createdBy!: string
  @Column({
      type : DataType.STRING,
  })
  updatedBy!: string
  @Column({
      type : DataType.BOOLEAN
  })
  isActive !: boolean
}

@Table({
  timestamps:true,
  tableName : "stringing"
})
export class stringing extends Model {
  @Column({
    type: DataType.STRING,
  })
  stringCapacity!: string;
  @Column({
    type: DataType.STRING,
  })
  lilt!: string;
  @Column({
    type: DataType.STRING,
  })
  azimuth!: string;
  @Column({
    type: DataType.INTEGER,
  })
  noOfModules!: number;
  @Column({
    type: DataType.INTEGER,
  })
  moduleList!: number;
  @Column({
    type: DataType.BOOLEAN,
  })
  isActive!: boolean;
  @Column({
    type: DataType.STRING,
  })
  createdBy!: string;
  @Column({
    type: DataType.STRING,
  })
  updatedBy!: string;
}

@Table({
  timestamps:true,
  tableName :"inverter"
})
export class inverter extends Model {

  @Column({
    type: DataType.STRING,
  })
  make!: string;
  @Column({
    type: DataType.STRING,
  //  defaultValue: true,
  })
  model!: string;
  @Column({
    type: DataType.STRING,
  //  defaultValue: true,
  })
  acCapacity!: string;
  @Column({
    type: DataType.STRING,
  //  defaultValue: true,
  })
  serialNumber!: string;
  @Column({
    type: DataType.STRING,
  //  defaultValue: true,
  })
  bisNumber!: string;
  @Column({
    type: DataType.DATE,
  })
  commisionDate!: Date;
  @Column({
    type: DataType.DATE,
  })
  decommisionDate!: Date;
  @Column({
    type: DataType.STRING,
  })
  stringList!: string;
  @Column({
    type: DataType.BOOLEAN,
  })
  isActive!: boolean;
  @Column({
    type: DataType.STRING,
  })
  createdBy!: string;
  @Column({
    type: DataType.STRING,
  })
  updatedBy!: string;
}
@Table ({
  timestamps :true,
  tableName :"plantDocumentType"
})
export class plantDocumentType extends Model {
  @Column({
    type: DataType.STRING,
  })
  plantDocumentType!: string;
  @Column({
    type: DataType.BOOLEAN,
  })
  isActive!: boolean;
}
@Table ({
  timestamps : true,
  tableName : 'PlantDocuments'
})
export class plantDocument extends Model {
  @Column({
    type: DataType.STRING,
  })
  plantId!: string;
  @Column({
    type: DataType.STRING,
  })
  documentTypeId!: string;
  @Column({
    type: DataType.STRING,
  })
  documentUrl!: string;
  @Column({
    type: DataType.DATE,
  })
  lastUploadedOn!: Date;
  @Column({
    type : DataType.STRING
  })
  lastUploadedBy !: String
  @Column({
    type : DataType.BOOLEAN
  })
  isActive !: Boolean

}