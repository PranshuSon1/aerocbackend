import express from "express";
import { urlencoded, json } from "body-parser";
import Dotenv from "dotenv";
import router from "./routes/routes";
import connect from "./common/database";
import config from "./config/config";
import logging from "./config/logging";
const NAMESPACE = 'DATABASE CONNECTION';
const app = express();
Dotenv.config();
app.set('secretKey', process.env.APP_SECRET as string)
const port = Number(process.env.PORT) as Number;
app.use(urlencoded({ extended: true }));
app.use(json());
app.use(express.static(__dirname + "/"));
app.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
  res.status(500).json({ message: err.message });
});

app.use('/app', router);

app.listen(port, async () => {
  console.log("listning at port :" + port)
  await connect(config.sql)
});
