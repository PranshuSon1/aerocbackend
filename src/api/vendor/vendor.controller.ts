import { Request,Response,NextFunction } from "express";
import { createVendor,getallVendor,updateVendor } from "./vendor.service";

export const addVendor = async (req :Request, res:Response) : Promise<void> =>{
    await createVendor(req,(error : any , result :any)=>{
        if(error){
            res.status(200).json({error : error.message})
        }else{
            res.json({result})
        }
    })
}
export const getallVendors = async (req :Request, res:Response) : Promise<void> =>{ 
    await getallVendor(res, (error :any , result : any)=>{
        if(error){
            res.status(200).json({error : error.message})
        }else{
            res.json({result})
        }
    })
}
export const updateVendors = async (req :Request, res:Response) : Promise<void> =>{ 
    await updateVendor(res, (error :any , result : any)=>{
        if(error){
            res.status(200).json({error : error.message})
        }else{
            res.json({result})
        }
    })
}