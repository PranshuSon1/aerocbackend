import { Table, Model, Column, DataType } from "sequelize-typescript";
@Table({
    timestamps: true,
    tableName: "vendors",
  })
  export class vendors extends Model { 
    @Column({
        type: DataType.STRING,
      //  defaultValue: true,
      })
      vendorName!: string;
    @Column({
        type: DataType.STRING,
      //  defaultValue: true,
      })
      vendorAddress!: string;
    @Column({
        type: DataType.STRING,
      //  defaultValue: true,
      })
      city!: string;
    @Column({
        type: DataType.STRING,
      //  defaultValue: true,
      })
      state!: string;
    @Column({
        type: DataType.STRING,
      //  defaultValue: true,
      })
      postCode!: string;
    @Column({
        type: DataType.STRING,
      //  defaultValue: true,
      })
      country!: string;
    @Column({
        type: DataType.INTEGER,
      //  defaultValue: true,
      })
      phone!: number;
      @Column({
        type: DataType.STRING,
      //  defaultValue: true,
      })
      email!: string;
      @Column({
        type: DataType.STRING,
      //  defaultValue: true,
      })
      contactPerson!: string;
      @Column({
        type: DataType.STRING,
      //  defaultValue: true,
      })
      type!: string;
      @Column({
        type: DataType.STRING,
      //  defaultValue: true,
      })
      createdBy!: string;
      @Column({
        type: DataType.STRING,
      //  defaultValue: true,
      })
      updatedBy!: string;
  }
@Table({
  timestamps: true,
  tableName: "vendorType",
})
export class vendorType extends Model { 
    @Column({
        type: DataType.STRING,
      //  defaultValue: true,
      })
      type!: string;
    @Column({
        type: DataType.STRING,
      //  defaultValue: true,
      })
      description!: string;
    @Column({
        type: DataType.BOOLEAN,
      //  defaultValue: true,
      })
      isActive!: boolean;
}