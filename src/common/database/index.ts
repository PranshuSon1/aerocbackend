import { Sequelize, Model } from "sequelize-typescript";
import { Users } from "../../api/user/user.model";
import { usersRoles } from "../../api/userRole/userRole.model";
import {
  plants,
  plantType,
  plantLocationType,
  _module,
  consumptionType,
  siteManager,
  stringing,
  inverter,
  plantDocument,
  plantDocumentType,
} from "../../api/plants/plants.models";
import Dotenv from "dotenv";
import logging from "../../config/logging";

const NAMESPACE = 'DATABASE CONNECTION';
Dotenv.config();

type dbType = {
  sequelize?: any,
  Sequelize?: any,
  users?: any,
  plants?: any
}
const db: dbType = {};

const connect = async (database: any): Promise<void> => {
  logging.info(NAMESPACE, `Attempting to connect Database`);
  try {
    const sequelize = new Sequelize({
      host: process.env.DATABASE_HOST,
      port: Number(process.env.DATABASE_PORT),
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_NAME,
      dialect: 'postgres',
      logging: false,
      models: [Users,
        usersRoles,
        plants,
        plantType,
        plantLocationType,
        _module,
        consumptionType,
        siteManager,
        stringing,
        inverter,
        plantDocument,
        plantDocumentType,],
    });
    db.sequelize = sequelize;
    db.Sequelize = Sequelize;
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
    await sequelize.sync({ force: false }).then(() => {
      console.log('database Synced')
    })
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}
export default connect