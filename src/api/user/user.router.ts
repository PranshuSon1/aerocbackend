import { Router } from "express";
import {addUser, getall,login} from './user.controller'
import validateUser from "../../common/middilewares/validateUser";
const usersRouter = Router();


usersRouter.post('/login', login)
usersRouter.post('/add' , validateUser,addUser);
usersRouter.get('/getall', validateUser,getall);


export default usersRouter;
