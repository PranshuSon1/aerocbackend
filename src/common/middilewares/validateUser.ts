import jwt from "jsonwebtoken";
import { Request,Response,NextFunction} from "express";
import logging from '../../config/logging';
import config from "../../config/config";
const NAMESPACE = 'User';
/**
 * middleware to check whether user has access to a specific endpoint 
 * 
 * @param validateUser list of allowed access types of a specific endpoint
 */

 const validateUser = (req: Request, res: Response, next: NextFunction) => {
    logging.info(NAMESPACE, 'Validating token');
    let token = req.headers.authorization?.split(' ')[1];
    if (token) {
        jwt.verify(token, config.server.token.secret, (error, decoded) => {
            if (error) {
                return res.status(404).json({
                    message: error,
                    error
                });
            } else {
                res.locals.jwt = decoded;
                next();
            }
        });
    } else {
        return res.status(401).json({
            message: 'Unauthorized'
        });
    }

  };

export default validateUser