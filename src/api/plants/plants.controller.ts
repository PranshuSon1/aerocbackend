import { Request,Response,NextFunction } from "express";
import { createPlant,getAllPlants,updatePlants } from "./plants.service";

export const addPlant = async (req :Request, res:Response) : Promise<void> =>{
    await createPlant(req,(error : any , result :any)=>{
        if(error){
            res.status(200).json({error : error.message})
        }else{
            res.json({result})
        }
    })
}
export const getPlant = async (req :Request, res:Response) : Promise<void> =>{ 
    await getAllPlants(res, (error :any , result : any)=>{
        if(error){
            res.status(200).json({error : error.message})
        }else{
            res.json({result})
        }
    })
}
export const updateVendors = async (req :Request, res:Response) : Promise<void> =>{ 
    await updatePlants(res, (error :any , result : any)=>{
        if(error){
            res.status(200).json({error : error.message})
        }else{
            res.json({result})
        }
    })
}