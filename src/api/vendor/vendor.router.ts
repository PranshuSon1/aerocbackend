import { Router } from "express";
import validateUser from "../../common/middilewares/validateUser";
import { addVendor,getallVendors,updateVendors } from "./vendor.controller";

const vendorRouter = Router();

vendorRouter.get('/getall',validateUser , getallVendors);
vendorRouter.post('/add',validateUser, addVendor);
vendorRouter.put('/update',validateUser,updateVendors);

export default vendorRouter