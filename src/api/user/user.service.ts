import { Users } from "./user.model"
import { genSaltSync,hashSync,compareSync } from "bcryptjs";
import config from "../../config/config";
import logging from "../../config/logging";
import { sign } from "jsonwebtoken";


export const create = async (req : any ,callBack :any): Promise<any> => { 
try {
    // console.log('Hello!')
    let data = {...req.body};
    let salt = genSaltSync(10);
    data.userCredentials = hashSync(data.userCredentials,salt)
    let userData = await Users.create(data);
    await userData.save();
    return callBack (null , {message :'User created successfully',data :userData}) 
} catch (error :any) {
    console.log('error', error)
    return callBack(true,error)
}
};
export const authenticate = async (req:any,callBack:any):Promise<any>=>{
    try {
        let userData = await Users.findOne({where:{userName:req.body.userName}});
        if (userData!==null && compareSync(req.body.userCredentials,userData.userCredentials)) {
            const {userName , userCredentials} = userData;
            // let Token =sign({userName},req.app.get('secretKey'),{expiresIn : '2h'})
            signJWT(userData, (_error, token) => {
                if (_error) {
                    return callBack( null , {  message: _error.message,error: _error})
                } else if (token) {
                    return callBack ( null , {message: 'Auth successful',token: token,user: userData})
                }
            })
        } else {
            return callBack(null,{status : 'login Failed',Data:null,token:null})
        }
    } catch (error) {
        console.log('error', error)
        return callBack(error)
    }
}
export const findAll = async (req :Request , callBack:any) : Promise<any> =>{
    try {
        // console.log('req', req.body)
        let data = await Users.findAll({})
        // console.log('data', data)
        return callBack (null , {data : data}) 
    } catch (error :any) {
        console.log('error', error)
        return callBack(error)
    }
}


const signJWT = (user:any, callBack : (error : Error | null, token : string|null)=> void) : void =>{
    const NAMESPACE = 'Auth';
    var timeSinceEpoch = new Date().getTime();
    var expirationTime = timeSinceEpoch + Number(config.server.token.expireTime) * 100000;
    var expirationTimeInSeconds = Math.floor(expirationTime / 1000);
    logging.info(NAMESPACE, `Attempting to sign token for ${user._id}`);
    try {
        sign(
            {
                username: user.username
            },
            config.server.token.secret,
            {
                issuer: config.server.token.issuer,
                algorithm: 'HS256',
                expiresIn: expirationTimeInSeconds
            },
            (error, token) => {
                if (error) {
                    callBack(error, null);
                } else if (token) {
                    callBack(null, token);
                }
            }
        );
    } catch (error : any) {
        logging.error(NAMESPACE, error.message, error);
        callBack(error, null);
    }

}