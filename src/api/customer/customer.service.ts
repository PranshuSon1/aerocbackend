import { customer } from "./customer.model";
import logging from "../../config/logging";
const NAMESPACE = 'Customer'; 

export const create = async (req : any,callBack :any) :Promise<any> =>{
    try {
        let data = {...req.body};
        let vendorData = await customer.create(data)
        await vendorData.save();
        return callBack(null,{message : 'created Successfully', data: vendorData})
    } catch (error :any) {
        console.log('error', error)
        return callBack(true,error)
    }
}
export const getall = async (req : any,callBack :any) :Promise<any> =>{
    try {
        let data = {...req.params};
        let customerData = await customer.findAll({})
        return callBack(null,{data: customerData})
    } catch (error :any) {
        console.log('error', error)
        return callBack(true,error)
    }
}