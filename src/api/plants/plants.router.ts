import { Router } from "express";
import validateUser from "../../common/middilewares/validateUser";
import { addPlant,getPlant } from "./plants.controller";
const plantsRouter = Router();

plantsRouter.get('/getall',validateUser,getPlant);
plantsRouter.post('/add',validateUser,addPlant);

export default plantsRouter