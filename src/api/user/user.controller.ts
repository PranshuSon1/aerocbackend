import { Request, Response ,NextFunction} from "express"
// import { Error } from "sequelize";
import { create ,findAll, authenticate} from "./user.service";


// const NAMESPACE = 'User';
export const addUser = async (req: Request, res: Response): Promise<void> => {

  await create(req, (error: Error, result: any) => {
    if (error) {
       res.status(400).json({ error: error.message })
    } else {
       res.json({ result })
    }
  })
}

export const getall = async (req: any, res: Response): Promise<void> => {
 
  await findAll(req,(error:Error,result:any)=>{
    if (error) {
     res.status(400).json({error:error.message})
    } else {
     res.json({result})
    }
  })
}

export const login =async (req:Request, res : Response ): Promise<void> =>{ 
 await authenticate(req,(error :Error , result : any) =>{
    if(error){
      // return
       res.status(400).json({error : error.message})
    }else {
      res.json({result})
    }
  })
}


